import { Component, OnInit } from '@angular/core';
import { HeroeModel } from 'src/app/models/hero.model';
import { NgForOf } from '@angular/common';
import { NgForm } from '@angular/forms';
import { HeroesService } from 'src/app/services/heroes.service';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { compileNgModule } from '@angular/compiler';
@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {
  heroe=new HeroeModel();
  
  constructor(private heroeService:HeroesService,
              private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    const id=this.route.snapshot.paramMap.get('id');
    if (id!=='nuevo') {
      this.heroeService.getById(id).subscribe((resp:HeroeModel)=>{
       this.heroe=resp;
       this.heroe.id=id;
      })
      
    }
  }

  guardar(form:NgForm){
    if(form.invalid){
      console.log('formulario no valido');
      return;
    }
    Swal.fire({
      title:'Espere',
      text:'Guardando informacion',
      icon:'info',
      allowOutsideClick:false
    });
    Swal.showLoading();
    let peticion:Observable<any>;

    if (this.heroe.id) {
      peticion=this.heroeService.UpdateHero(this.heroe);
      
    
    }else{
      peticion= this.heroeService.crearHero(this.heroe);
    }

    peticion.subscribe(res=>{
      Swal.fire({
        title:this.heroe.nombre,
        text:'se actualizo conrrectamente',
        icon:'success'
      }).then(item=>{
        this.router.navigateByUrl('/heroes')
      });
    });


    

  }
}
