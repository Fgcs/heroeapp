import { Component, OnInit } from '@angular/core';
import { HeroesService } from 'src/app/services/heroes.service';
import { HeroeModel } from 'src/app/models/hero.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-heros',
  templateUrl: './heros.component.html',
  styleUrls: ['./heros.component.css']
})
export class HerosComponent implements OnInit {

    heroes:HeroeModel[]=[];
    cargando:boolean=false;
  constructor(private heroesServices:HeroesService) { }

  ngOnInit(): void {
    this.cargando=true;

    this.heroesServices.getAll().subscribe(resp=>{
      this.heroes=resp
      this.cargando=false;
    });
  }

  deleteHero(heroe:HeroeModel,i:number){

    Swal.fire({
      title:'¿Esta seguro?',
      text:`¿Esta seguro que desea borrar a ${heroe.nombre}?`,
      icon:'question',
      showConfirmButton:true,
      showCancelButton:true
    }).then(resp=>{
      if(resp.value){

         this.heroes.splice(i,1);
         this.heroesServices.Delete(heroe.id).subscribe();

      }
    });
   

  }

}
