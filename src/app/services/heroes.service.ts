import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeroeModel } from '../models/hero.model';
import {map ,delay} from 'rxjs/operators';
import { pipe } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  private url='https://crud-45d38.firebaseio.com';
  constructor(private http:HttpClient) { }

  crearHero(heroe:HeroeModel){

    return this.http.post(`${this.url}/heroes.json`,heroe)
          .pipe(
            map((resp:any )=>{
              heroe.id=resp.name;
              
              return heroe;
            })
          );
  }
  UpdateHero(heroe:HeroeModel){
    
    const heroeTemp={
      ...heroe
    }
    delete heroeTemp.id;
    return this.http.put(`${this.url}/heroes/${heroe.id}.json`,heroeTemp);
  }
  getById(id:string){
      return this.http.get(`${this.url}/heroes/${id}.json`);
  }

  getAll(){
    return this.http.get(`${this.url}/heroes.json`)
              .pipe(
                map(this.crearArreglo),
                delay(1500)
              );
  }
  private crearArreglo(heroesObj:Object){
    const heroes:HeroeModel[]=[];

    if(heroesObj==null){return [];}
    Object.keys(heroesObj).forEach(key=>{
      const heroe:HeroeModel=heroesObj[key];
      heroe.id=key;
      heroes.push( heroe );
    });
    return heroes;
  }
  Delete(id:string){
    return this.http.delete(`${this.url}/heroes/${id}.json`);
  }

}
